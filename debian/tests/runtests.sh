#!/bin/sh

set -e

rm -f /tmp/pwn

cd debian/tests/files

php -S 127.0.0.1:8080 2>/dev/null &

cat <<EOF | expect
spawn commix -u http://127.0.0.1:8080/vuln.php?cmd=inject --all --ignore-session --batch
send "ls -l\n"
expect
send "cp /etc/passwd /tmp/pwn\n"
expect
send "quit\n"
expect
EOF

pkill php


echo
ls -l /tmp/pwn
test -f /tmp/pwn
